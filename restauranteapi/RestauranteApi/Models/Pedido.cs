﻿using Newtonsoft.Json;
using RestauranteApi.Clases;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace RestauranteApi.Models
{
    /// <summary>
    /// Modelo principal para el manejo de pedidos de los restaurantes
    /// </summary>
    public class Pedido
    {
        [Required()]
        public int Id { get; set; }

        [Required()]
        public int Cliente { get; set; }

        [Required()]
        public int Restaurante { get; set; }

        [Required()]
        public int Menu { get; set; }

        Bitacora bitacora = new Bitacora();
        /// <summary>
        /// Metodo encargado de insertar el pedido en la base de datos de pedidos
        /// </summary>
        /// <returns>Retorna un String con el Json del Resultado del Ingreso.</returns>
        /// 
        public string ingresarPedido(){
            Resultado resultado = new Resultado();
            var rand = new Random();
            resultado.CodigoResultado = rand.Next(1, 10);
            resultado.MensajeResultado = "Pedido Ingresado Correctamente!";
            bitacora.escribirBitacora("Se ingreso el pedido " + this.Id);
            return JsonConvert.SerializeObject(resultado); 
        }

        /// <summary>
        /// Metodo que consulta el estado del pedido del restaurante asignado
        /// </summary>
        /// <param name="pedido">id del pedido a consultar el estado</param>
        /// <returns>Retorna un String con el Json del Resultado del Estado.</returns>
        public string statusPedido(int pedido)
        {
            Resultado resultado = new Resultado();
            var rand = new Random();
            resultado.CodigoResultado = rand.Next(1, 4);
            if (resultado.CodigoResultado == 1)
            {
                resultado.MensajeResultado = "Pedido Ingresado!";
            }
            else if (resultado.CodigoResultado == 2)
            {
                resultado.MensajeResultado = "Preparando Menú!";
            }
            else
            {
                Repartidor repartidor = obtenerRepartidorDisponible();
                repartidor.Notificar(pedido);
                resultado.MensajeResultado = "Entregado a Repartidor!";
            }
            bitacora.escribirBitacora("El estado del pedido " + pedido + " es " + resultado.MensajeResultado);
            return JsonConvert.SerializeObject(resultado);
        }

        /// <summary>
        /// Metodo que obtiene el repartidor disponible para poder asignar el pedido finalizado por el restaurante para dar el envio
        /// </summary>
        /// <returns>Reporta un objeto de tipo Repartidor</returns>
        public Repartidor obtenerRepartidorDisponible()
        {
            var rand = new Random();
            var repartidor = new Repartidor();
            repartidor.Id = rand.Next(1, 50);
            repartidor.Nombre = "Repartidor " + repartidor.Id.ToString();
            return repartidor;
        }

    }
}
