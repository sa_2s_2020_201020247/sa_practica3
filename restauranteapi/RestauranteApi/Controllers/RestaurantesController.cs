﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using RestauranteApi.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

/// <summary>
/// Controlador principal para manejar los procedimientos de las acciones para un restaurante y sus pedidos
/// </summary>
namespace RestauranteApi.Controllers
{
    [Consumes("application/json", "application/json-patch+json", "multipart/form-data")]
    [Route("api/[controller]")]
    public class RestaurantesController : Controller
    {
        /// <summary>
        /// Metodo GET que unicamente retorna un texto para validar si el servicio esta activo
        /// </summary>
        /// <returns>Retorna un texto con un mensaje del nombre del servicio y su fecha</returns>
        [HttpGet("/status")]
        public string status()
        {
            return "Microservicio de Restaurantes - Software Avanzado Segundo Semestre 2020 - " + DateTime.Now.ToString();
        }

        /// <summary>
        /// Metodo GET que consulta el estado de un pedido de un restaurante
        /// </summary>
        /// <param name="pedido">Id del pedido que se consultará</param>
        /// <returns>Retorna un JSON con el resultado obtenido de la operacion </returns>
        [HttpGet("/getStatusPedido")]
        public ActionResult getStatusPedido(int pedido)
        {
            Pedido idPedido = new Pedido();
            return Content(idPedido.statusPedido(pedido), "application/json");
        }

        /// <summary>
        /// Metodo que inserta un pedido en la base de datos de restuarantes.
        /// </summary>
        /// <param name="request">Body con el contenido del Pedido</param>
        /// <returns>Retorna un JSON con el resultado obtenido de la operacion </returns>
        [HttpPost, Route("/IngresarPedido")]
        public ActionResult ingresarPedio([FromBody] Pedido request)
        {

            if (!ModelState.IsValid)
            {
                var message = JsonConvert.SerializeObject(BadRequest(ModelState).Value);
                return Content("{\"codigoResultado\":-1,\"mensajeResultado\":[" + JsonConvert.SerializeObject(BadRequest(ModelState).Value) + "]}", "application/json");
            }

            return Content(request.ingresarPedido(), "application/json");
        }

    }
}
