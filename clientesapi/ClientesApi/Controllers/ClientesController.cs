﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using ClientesApi.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

/// <summary>
/// Controlador principal para manejar los procedimientos de las acciones para un cliente y sus pedidos
/// </summary>
namespace ClientesApi.Controllers
{
    [Consumes("application/json", "application/json-patch+json", "multipart/form-data")]
    [Route("api/[controller]")]
    public class ClientesController : Controller
    {
        /// <summary>
        /// Metodo GET que unicamente retorna un texto para validar si el servicio esta activo
        /// </summary>
        /// <returns>Retorna un texto con un mensaje del nombre del servicio y su fecha</returns>
        [HttpGet("/status")]
        public string status()
        {
            return "Microservicio de Clientes - Software Avanzado Segundo Semestre 2020 - " + DateTime.Now.ToString();
        }

        /// <summary>
        /// Metodo que consulta el estado del pedido del restaurante asignado
        /// </summary>
        /// <param name="pedido">id del pedido a consultar el estado</param>
        /// <returns>Retorna un String con el Json del Resultado del Estado.</returns>
        [HttpGet("/getStatusRestaurante")]
        public ActionResult getStatusRestaurante(int pedido)
        {
            Pedido idPedido = new Pedido();
            return Content(idPedido.statusRestaurante(pedido), "application/json");
        }

        /// <summary>
        /// Metodo GET que consulta el estado de un pedido de un repartidor
        /// </summary>
        /// <param name="pedido">Id del pedido que se consultará</param>
        /// <returns>Retorna un JSON con el resultado obtenido de la operacion </returns>
        [HttpGet("/getStatusRepartidor")]
        public ActionResult getStatusRepartidor(int pedido)
        {
            Pedido idPedido = new Pedido();
            return Content(idPedido.statusRepartidor(pedido), "application/json");
        }

        /// <summary>
        /// Metodo que inserta un pedido.
        /// </summary>
        /// <param name="request">Body con el contenido del Pedido</param>
        /// <returns>Retorna un JSON con el resultado obtenido de la operacion </returns>
        [HttpPost, Route("/IngresarPedido")]
        public ActionResult ingresarTicket([FromBody] Pedido request)
        {

            if (!ModelState.IsValid)
            {
                var message = JsonConvert.SerializeObject(BadRequest(ModelState).Value);
                return Content("{\"codigoResultado\":-1,\"mensajeResultado\":[" + JsonConvert.SerializeObject(BadRequest(ModelState).Value) + "]}", "application/json");
            }

            return Content(request.sendPedido(), "application/json");
        }

    }
}
