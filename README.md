# PRACTICA 3

_APIS elaborados en .Net Core en su versión 2.2, en cada carpeta se tiene el código fuente de cada micro servicio (REST), la estructura básica esta en el unico controlador que tiene cada servicio y en sus modelos se encuentra la comunicación con otros servicios o la simulación de insercion o actualizacion de datos_

## Información General
- Practica 3 Laboratorio Software Avanzado
- Creado por Haroldo Arias
- Carnet 201020247
- Agosto 2020

## Video de Demostración
_Video de la demostración de la funcionalidad de estos servicios, como una rápida explicacion de su estructura_
* [Video](https://drive.google.com/file/d/1L1Y8_1LMJS9uGvPngIYIBf2ZYR2hGX7a/view?usp=sharing) - Acá puedes verlo

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

### Instalación 🔧

_La instalación es muy sencilla, si tienes visual studio en su versión 2019 solo basta con abrirla con el IDE y ejecutarla, caso contrario te explicó el procedimiento_

### Pre-requisitos 📋
_Debes de tener instalado el SDK del .NetCore en su versión 2.2_
* [.Net Core 2.2](https://dotnet.microsoft.com/download/dotnet-core/2.2) - Acá puedes descargarlo

## Compilación del Servicio ⚙️
_Tienes que ingresar en cada una de las carpetas por separado, Si se utiliza el IDE del Visual Studio solo basta con Compilarlo dentro del IDE_
Manualmente hay que correr el siguiente comando
```
dotnet build
``` 

## Ejecución del Servicio
_Si se utiliza el IDE del Visual Studio solo basta con Ejecutarlo dentro del IDE_
Manualmente hay que correr el siguiente comando

_Servicio de Clientes, correrá en el puerto 8081_

```
dotnet run --project=ClientesApi
``` 
_Servicio de Restaurantes, correrá en el puerto 8082_

```
dotnet run --project=RestauranteApi
```
_Servicio de Repartidores, correrá en el puerto 8083_

```
dotnet run --project=RepartidorApi
```
## Autor ✒️

* **Haroldo Pablo Arias Molina** - *Trabajo Inicial* - [harias25](https://github.com/harias25)
 
## Licencia 📄

Este proyecto está bajo la Licencia Libre - mira el archivo [LICENSE.md](LICENSE.md) para detalles